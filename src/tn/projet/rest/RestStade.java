package tn.projet.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.iset.worldcup.ejb.services.GestionStadeWRemote;
import useruser.entities.Stade;





@Path("stade")
public class RestStade {
	@EJB
	private GestionStadeWRemote geststade ; 
	
	
	@POST
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON} )
	@Path(value = "addstade")
	public Response AjoutStade(Stade stade) { 
		geststade.addstade(stade);  
		return Response.status(Status.CREATED).entity("Ajout Stade avec succes").build();
	}
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("modifstade")
	public Response modifStade(Stade stade) {  
		geststade.updatestade(stade);    
		return Response.status(Status.OK).entity("update succesfully").build();
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteEtud(@PathParam("id") String cin) {
		Stade s = geststade.findstadeID(Integer.parseInt(cin));
		geststade.deletestade(s);
		return Response.status(Status.OK).entity("Remove").build();

	}

	
	
 
	@GET 
	@Produces(MediaType.APPLICATION_JSON)
	@Path("aff")
	public Response getstades() {
		List<Stade> stade =   geststade.findAllstade() ;
		if (!stade.isEmpty()) {
			return Response.ok(stade).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path(value="json")
	public List<Stade> getstadesList(){
		return   geststade.findAllstade() ;  
	}
	
}
