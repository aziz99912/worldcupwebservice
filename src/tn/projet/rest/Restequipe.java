package tn.projet.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.iset.worldcup.ejb.service.GestionEquipeERemote;

import useruser.entities.Equipe;
import useruser.entities.user;



@Path("equipe")
public class Restequipe {
	@EJB
	private GestionEquipeERemote gestequipe ; 
	@POST
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON} )
	@Path(value = "addequipe")
	public Response Ajoutequipe(Equipe equipe) { 
		gestequipe.addequipe(equipe);  
		return Response.status(Status.CREATED).entity("Ajout  avec succes").build();
	}
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("modifequipe")
	public Response modifEquipe(Equipe equipe) {
		gestequipe.updateequipe(equipe);   
		return Response.status(Status.OK).entity("update succesfully").build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getusers() {
		List<Equipe> stade = gestequipe.findAllequipe();
		if (!stade.isEmpty()) {
			return Response.ok(stade).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	

	@DELETE
	@Path("{id}")
	public Response deleteequipe(@PathParam("id") String id) {
		Equipe e = gestequipe.findequipeID(Integer.parseInt(id));
		gestequipe.deleteequipe(e);
		return Response.status(Status.OK).entity("Remove").build();

	}

}
