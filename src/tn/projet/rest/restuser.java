package tn.projet.rest;

import java.util.List;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.iset.worldcup.ejb.servicees.GestionuserURemote;


import useruser.entities.user;

@Path("/user")
public class restuser {
	@EJB
	private GestionuserURemote gestuser;
	user user = new user();
	List <user> users = new ArrayList<user>();
	
	@GET
	@Path("/login/{email}/{password}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String login(@PathParam("email") String email , @PathParam("password") String password) {
		
	    if (email.isEmpty()|| password.isEmpty())
	    {
	    	return "failed";
	    	
	    }
	    else {
	    	return   gestuser.login(email , password) +" "+ "login success";
	    }
}
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response Ajoutuser(user user) {
		gestuser.adduser(user);
		return Response.status(Status.CREATED).entity("Ajout user avec succes").build();
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifuser(user user) {
		gestuser.updateuser(user);
		return Response.status(Status.OK).entity("update succesfully").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/user")
	public Response getusers() {
      return  Response.status(Status.ACCEPTED).entity(this.users).header("Access-Control-Allow-Origin","*").allow("OPTIONS").build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{id}")
	public user getEtud(@PathParam("id") String id) {
		return gestuser.finduserID(Integer.parseInt(id));

	}  

	@DELETE
	@Path("{id}")
	public Response deleteEtud(@PathParam("id") String id) {
		user u = gestuser.finduserID(Integer.parseInt(id));
		gestuser.deleteuser(u);
		return Response.status(Status.OK).entity("Remove").build();

	}


}
