package tn.projet.rest;

import java.util.List;


import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.iset.worldcup.ejb.services.match.GestionmatchRemote;
import useruser.entities.Matches;
import useruser.entities.Stade;





@Path("match")
public class RestMatch {
	@EJB
	private GestionmatchRemote gestmatch ; 
	
	
	@POST
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON} )
	@Path(value = "addMatch")
	public Response AjoutStade(Matches Match) { 
		gestmatch.addmatch(Match);  
		return Response.status(Status.CREATED).entity("Ajout Match avec succes").build();
	}
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("modifMatch")
	public Response modifStade(Matches Match) {  
		gestmatch.updatematch(Match);    
		return Response.status(Status.OK).entity("update succesfully").build();
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteEtud(@PathParam("id") String cin) {
		Matches s = gestmatch.findmatchID(Integer.parseInt(cin));
		gestmatch.deletematch(s);
		return Response.status(Status.OK).entity("Remove").build();

	}

	
	
 
	@GET 
	@Produces(MediaType.APPLICATION_JSON)
	@Path("aff")
	public Response getstades() {
		List<Matches> Match =   gestmatch.findAllmatch() ;
		if (!Match.isEmpty()) {
			return Response.ok(Match).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path(value="json")
	public List<Matches> getstadesList(){
		return   gestmatch.findAllmatch() ;  
	}
	
}
